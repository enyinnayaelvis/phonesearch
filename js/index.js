/*	Index module and controller for the index.php page 	*/
/*	Checklist module gotten from githb	*/
var IndexApp = angular.module('IndexApp', ["checklist-model"]);
IndexApp.controller('IndexController', function($scope, $window, $http, $log, $httpParamSerializerJQLike){
	$scope.navChecked = false;					//to toggle debug window
	$scope.queryArray = $window.jsQueryArray;	//data returned to page on startup.. atm empty

	/* Array contains selected items and data
	* to be passed to the server for search
	 */
	$scope.ParamList = {
		"price" : {
			"type" : "range",
			"max" : 700,
			"min" : 0,
		},
		"f_camera" : {
			"type" : "range",
			"max" : 30,
			"min" : 0,
		},
		"brand" : {
			"type" : "checkbox",
			"choice" : [
				"Apple",
			],
		},
		"year" : {
			"type" : "checkbox",
			"choice" : {},
		},
		"series" : {
			"type" : "checkbox",
			"choice" : {},
		},
		"name" : {
			"type" : "checkbox",
			"choice" : {},
		},
		"device" : {
			"type" : "checkbox",
			"choice" : {},
		}
	};

	/**
	 * Performs AJAX request to the server
	 * and returns search result to the page
	 */
	$scope.Search = function(){
		$http({
			method : 'POST',
			url : '/phonesearch/php/Index/IndexPost.php',
			data : $httpParamSerializerJQLike({
				"user_params" : $scope.ParamList
			}),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}

		}).then(function(response){
			$scope.newName = response.data;
			$scope.queryResult = response.data;
			$log.info(response);
		}, function(reason){
			$scope.reason = reason.data;
			$scope.newName = reason.data;
			$log.info(reason);
		})};


	/**
	 * Toggles debug window on page
	 */
	$scope.navToggle = function(){
		if ( $scope.navChecked ){
			$scope.navChecked = false;
		}else{
			$scope.navChecked = true;
		}
	}
	
	
});
