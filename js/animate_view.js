$(document).ready(function(){
	
	var toggleView = true;
	/* 	toggle animate views based on button click 	*/
	$("#animButton").click(function(){
		var lb_left = parseInt($("#left_body").css('left'));
		var rb_left = parseInt($("#right_body").css('left'));
		if ( toggleView ){
			toggleView = !toggleView;
			lb_left += 700;
			rb_left -= 550;
			$("#left_body").animate({
				left : lb_left,
			});
			$("#right_body").animate({
				left : rb_left,
			}, {duration : 700, queue : false });
		}else{
			toggleView = !toggleView;
			lb_left -= 700;
			rb_left += 550;
			$("#left_body").animate({
				left : lb_left,
			});
			$("#right_body").animate({
				left : rb_left,
			}, {duration : 700, queue : false });
		}

	});
});