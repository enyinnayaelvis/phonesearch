<?php

namespace App\Requests;


/**
 * Class Request
 * @package App\Requests
 * Handles all incoming requests
 * mainly GET and POST requests
 */
class Request{

    /**
     * Request constructor.
     */
    public function __construct(){}

    /**
     * Returns incoming array from GET request
     */
    public function GetIncomingRequest(){
        return $_GET;
    }

    /**
     * Returns incoming array from POST request
     */
    public function PostIncomingRequest(){
        return $_POST;
    }
}