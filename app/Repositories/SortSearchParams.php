<?php

namespace App\Repositories;

use App\Repositories\CreateSearchQuery as CreateQuery;


/**
 * Class SortSearchParams
 * @package App\Repositories
 * Controller for the creation of the querystring for the database
 */
class SortSearchParams{

    /* PUBLIC VARIABLES */
    public $CreateQuery;
    public $queryString;
    public $propertiesArray = array("name", "device", "brand", "year", "series", "f_camera", "price");
    /* CONSTANT VARIABLES */
    const SELECTSTRING = "SELECT ";

    /**
     * SortSearchParams constructor.
     */
    public function __construct(){
        $this->CreateQuery = new CreateQuery;
    }

    /**
     *  Creates initial part of the query string
     *  what should be selected from the database
     */
    public function InitializeQueryString(){
        $this->queryString = self::SELECTSTRING;
        $this->queryString.= $this->CreateQuery->GetSelectionItems($this->propertiesArray);
        $this->queryString.= " FROM `phone_data` WHERE ( ";
        return $this->queryString;
    }
    
    /**
     *  Creates string for the options for selection
     *  - select products that match user selected item
     *  based on the type of selection
     */
    public function GetSearchParams($data){
        $queryString = "";
        $numCount = 0;
        foreach($data['user_params'] as $propertyKey => $propertyObj){
            if ( isset($propertyObj['choice']) || ($propertyObj['type'] == "range")  ) {
                $numCount += 1;
                if ($numCount > 1) {
                    $queryString .= "AND";
                } else if ($numCount == sizeof($data['user_params'])) {
                    $queryString .= " )";
                }
                if ($propertyObj['type'] == 'range') {
                    $queryString .= $this->CreateQuery->RangeSearch($propertyKey, $propertyObj);
                } else {
                    $queryString .= $this->CreateQuery->CheckboxSearch($propertyKey, $propertyObj);
                }
            }

        }
        $queryString.= " )";
        return $queryString;
    }

}