<?php

namespace App\Repositories;


/**
 * Class CreateSearchQuery
 * @package App\Repositories
 * Creates the strings for querying the database
 */
class CreateSearchQuery{
    /*  PUBLIC VARIABLES    */
    public $queryString;
    public $propertiesArray = array("name", "device", "brand", "year", "series", "f_camera", "price");
    /*  CONSTANT VARIABLES  */
    const SELECTSTRING = "SELECT ";

    /*  FUNCTIONS   */

    /**
     * @return string
     * creates the beginning statement for the query string
     * what items should be selected
     */
    public function InitializeQueryString(){
        $this->queryString = self::SELECTSTRING;
        $this->GetSelectionItems($this->propertiesArray);
        $this->queryString.=" FROM `phone_data` WHERE (";
        return $this->queryString;
    }

    /**
     * @param $propertiesArrayParam
     * @return string
     * Called from the InitializeQueryString()
     * creates string of items that should be selected from the db
     */
    public function GetSelectionItems($propertiesArrayParam)
    {
        $queryString = "";
        $arrayCount = 0;
        foreach ($propertiesArrayParam as $item) {
            $arrayCount += 1;
            $queryString.= "`".$item."` ";
            if ($arrayCount < count($propertiesArrayParam)) {
                $queryString.= ", ";
            } else {

            }
        }
        return $queryString;
    }


    /**
     * @param $rangeKey
     * @param $rangeObj
     * @return string
     * creates the WHERE option if the search is of type range
     * i.e between two quantities
     */
    public function RangeSearch($rangeKey, $rangeObj){
        if ( !isset($rangeObj['min']) ){
            $rangeObj['min'] = 0;
        }
        if ( !isset($rangeObj['max']) ){
            $rangeObj['max'] = 2000;
        }
        $queryString = "( ";
        $queryString.= "`".$rangeKey."`".">"."'".$rangeObj['min']."'";
        $queryString.= " AND "."`".$rangeKey."`"."<"."'".$rangeObj['max']."'";
        $queryString.= " )";
        return $queryString;
    }

    /**
     * @param $checkboxKey
     * @param $checkboxArray
     * @return string
     * creates the WHERE option if the search is of type checkbox
     * i.e if the user selects items using a checkbox
     */
    public function CheckboxSearch($checkboxKey, $checkboxArray){
        $queryString = "";
        if ( isset($checkboxArray['choice']) ){
            $queryString = "( ";
            $numCount = 0;
            foreach ( $checkboxArray['choice'] as $checkboxItem ){
                $numCount += 1;
                $queryString.= "`" . $checkboxKey . "`" . "=" . "'" . $checkboxItem . "'";
                if ( $numCount == sizeof($checkboxArray['choice']) ) {
                    $queryString.= " )";
                }else if ( $numCount < sizeof($checkboxArray['choice']) ){
                    $queryString.= " OR ";
                }
            }
        }
        return $queryString;
    }



}