<?php

namespace App\Repositories;

use App\Connection;

/**
 * Class MainSearch
 * @package App\Repositories
 * Performs the actual query of the database
 */
class MainSearch{

    /*  PROTECTED VARIABLES */
    protected $connection;
    protected $mysqli;
    
    /*  FUNCTIONS   */

    /**
     * MainSearch constructor.
     * Creates new connection to the Mysql database
     */
    public function __construct(){
        $this->connection = new Connection;
        $this->mysqli = $this->connection->GetMysqli();
    }

    /**
     * @param $queryString
     * @return array
     * performs the actual query of the database 
     * returns the array of result from the database.
     */
    public function QueryDatabase($queryString)
    {
        $newQueryArray = [];
        if ($query_result = $this->mysqli->query($queryString)) {
            $arrayNum = 0;
            while ($query_array = mysqli_fetch_assoc($query_result)) {
                $newQueryArray[$arrayNum]['name'] = $query_array["name"];
                $newQueryArray[$arrayNum]['device'] = $query_array["device"];
                $newQueryArray[$arrayNum]['brand'] = $query_array['brand'];
                $newQueryArray[$arrayNum]['year'] = $query_array['year'];
                $newQueryArray[$arrayNum]['series'] = $query_array['series'];
                $newQueryArray[$arrayNum]['f_camera'] = $query_array['f_camera'];
                $newQueryArray[$arrayNum]['price'] = $query_array['price'];
                $arrayNum += 1;
            }
        } else {
            echo "no result from array";
        }
        return $newQueryArray;

    }


}