<?php

namespace App;

/**
 * Class Connection
 * @package App
 * Connection to database
 */
class Connection{
	
	/*	PROTECTED VARIABLES	*/
	protected $mysql_host = "localhost";
	protected $mysql_user = "root";
	protected $mysql_password = "";
	protected $mysql_db = "phonesearch";
	protected $mysqli;
	/*	CONSTANT VARIABLES	*/
	const serverError = "could not connect to server";
	const databaseError = "could not connect to database";

	/**
	 * Connection constructor.
	 * Creates connection to mysql database 
	 * --using PHP MYSQLI
	 */
	public function __construct(){
		$this->mysqli = new \mysqli($this->mysql_host, $this->mysql_user, $this->mysql_password, $this->mysql_db);
		if ($this->mysqli->connect_errno){
			die(self::serverError);
		}else{

		}
	}

	/**
	 * @return \mysqli
	 * returns mysqli connection object for whenever needed.
	 */
	public function GetMysqli(){
		return $this->mysqli;
	}


	


}
?>
