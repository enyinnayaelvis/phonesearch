<?php

namespace App\Controllers;

use App\Repositories\MainSearch;
use App\Repositories\SortSearchParams as SortSearch;
use App\Repositories\CreateSearchQuery as CreateQuery;

/**
 * Class IndexController
 * @package App\Controllers
 * The IndexController handles all major logic
 * for the index page
 */
class IndexController
{
    /* PUBLIC VARIABLES */
    public $SortSearch;             //variable for SortSearch class
    public $MainSearch;             //variable for MainSearch class

    /* FUNCTIONS */
    public function __construct(){
        $this->SortSearch = new SortSearch;
        $this->MainSearch = new MainSearch;
    }


    /**
     * returns data to page from GET request
     */
    public function ReturnGetPageData($data){
        $pageData = $this->GetIndex($data);
        return $pageData;
    }

    /**
     *  Main controller for the incoming GET request
     *  Creates the query string and queries the database
     *  returning the result from the queried database
     */
    public function GetIndex($getData){
        return $getData;
    }


    /**
     *   data to page from POST request
     */
    public function ReturnPostPageData($data){
        $pageData = $this->PostIndex($data);
        return $pageData;
    }

    /**
     *  Main controller for the incoming POST request
     *  Creates the query string and queries the database
     *  returning the result from the queried database
     */
    public function PostIndex($postData){
        $queryString = $this->SortSearch->InitializeQueryString();
        $queryString.= $this->SortSearch->GetSearchParams($postData);
        $queryResult = $this->MainSearch->QueryDatabase($queryString);
        return $queryResult;
    }

    
}
