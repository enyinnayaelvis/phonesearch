<?php

use App\Requests\Request;
use App\Controllers\IndexController;

require_once('../../app/page_autoload.php');

/**
 * Get the Index Controller and Request Object
 * pass the incoming POST request to the Index Controller
 * and return the data to the page
 * -- all page logic happening in the index controller
 */
$IndexController = new IndexController();
$Request = new Request();

/**
 *  Data passed to page from any incoming GET request
 */
$postResult = $IndexController->ReturnPostPageData( $Request->PostIncomingRequest() );

echo json_encode($postResult);