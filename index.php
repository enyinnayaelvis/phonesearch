<?php
	require "php/Index/IndexStart.php";
?>
<html>
	<head>	
		<title>Ensearch</title>
		<link rel="stylesheet" type="text/css" href="css/index.css"/>
		<script src="js/lib/jquery.js" ></script>
		<script src="js/lib/angular.min.js" ></script>
		<script src="js/directives/checklist-model.js" ></script>
	</head>
	<body ng-app="IndexApp" ng-controller="IndexController" ng-init="Search()">
		<!-- PAGE HEADER -->
		<div id = "head" >
			<div id = "logo" >
				<input type = "image" id = "nav_drawer" src = "images/nav_drawer.png" alt = "button" ng-click="navToggle()" ></input>
				<header><img src = "images/ensearch_title.png"  /></header>
				<div id = "search_div" >
					<div title = "search penplay" id = "search_span" >
						<input type = "text"  id  = "input_search_text" placeholder = "search penplay" ></input>
						<input type = "image" id = "input_search_button" src = "images/search_icon.png" alt = "button" ></input>	
					</div>
				</div>
				<div id = "signup_div" >
					<div>
						<input type = "image" id = "input_signup_image" src = "images/signin_empty_icon.png" alt = "button" ></input>
						<span id = "signup_span" >Sign Up</span>
					</div>
				</div>	
			</div>
		</div>
		<!-- HIDDEN DIV GETS DATA FROM THE SERVE AND PASSES TO JS -->
		<div id="hiddenDiv" >
			<script  type='text/javascript'>
				var jsQueryArray = <?= json_encode($newQueryArray)."\n"; ?>;	
			</script>
		</div>
		<!-- MAIN BODY -->
		<div id="body" ng-cloak >
			<!-- ACTS AS OPTIONS WINDOW -->
			<div id = "left_body" >
				<div id = "middle_section" class="m_one" >
					<div class="categoryDiv" >
						<div class="innerCategoryDiv" >
							<div class="categoryNameDiv" >
								<span id="categoryPrice" >Checkbox Price</span>
							</div>
							MIN<input class="rangeClass" type="number" ng-model="ParamList.price.min" />
							<input class="rangeClass" type="number" ng-model="ParamList.price.max" />MAX £
							<input class="rangeFinish" type="button" value="Go" ng-click="Search()" />

						</div>
					</div>

					<div class="categoryDiv" >
						<div class="innerCategoryDiv" >
							<div class="categoryNameDiv" >
								<span id="categoryCamera" >Checkbox Camera</span>
							</div>
							MIN<input class="rangeClass" type="number" ng-model="ParamList.f_camera.min" />
							<input class="rangeClass" type="number" ng-model="ParamList.f_camera.max" />MAX pixels
							<input class="rangeFinish" type="button" value="Go" ng-click="Search()" />
						</div>
					</div>

					<div class="categoryDiv" >
						<div class="innerCategoryDiv" >
							<div class="categoryNameDiv" >
								<span id="categorySpeed" >Checkbox Series</span>
							</div>
							<div class="categoryOptionsDiv" >
								<div class="option" >
									<input class="speedClass" type="checkbox" value="a" checklist-model="ParamList.series.choice" ng-click="Search()" />A
									<input class="speedClass" type="checkbox" value="j" checklist-model="ParamList.series.choice" ng-click="Search()" />J
									<input class="speedClass" type="checkbox" value="m" checklist-model="ParamList.series.choice" ng-click="Search()" />M
									<input class="speedClass" type="checkbox" value="s" checklist-model="ParamList.series.choice" ng-click="Search()" />S
									<input class="speedClass" type="checkbox" value="note" checklist-model="ParamList.series.choice" ng-click="Search()" />Note
									<input class="speedClass" type="checkbox" value="nexus" checklist-model="ParamList.series.choice" ng-click="Search()" />Nexus
									<input class="speedClass" type="checkbox" value="on" checklist-model="ParamList.series.choice" ng-click="Search()" />On
									<input class="speedClass" type="checkbox" value="ascend" checklist-model="ParamList.series.choice" ng-click="Search()" />Ascend
									<input class="speedClass" type="checkbox" value="honor" checklist-model="ParamList.series.choice" ng-click="Search()" />Honor
								</div>

							</div>
						</div>
					</div>

					<div class="categoryDiv" >
						<div class="innerCategoryDiv" >
							<div class="categoryNameDiv" >
								<span id="categoryYear" >Checkbox Year</span>
							</div>
							<div class="categoryOptionsDiv" >
								<div class="option" >
									<input class="yearClass" type="checkbox" value="2013" checklist-model="ParamList.year.choice" ng-click="Search()" />2013
									<input class="yearClass" type="checkbox" value="2014" checklist-model="ParamList.year.choice" ng-click="Search()" />2014
									<input class="yearClass" type="checkbox" value="2015" checklist-model="ParamList.year.choice" ng-click="Search()" />2015
								</div>

							</div>
						</div>
					</div>

					<div class="categoryDiv" >
						<div class="innerCategoryDiv" >
							<div class="categoryNameDiv" >
								<span id="categoryBrand" >Checkbox Brand</span>
							</div>
							<div class="categoryOptionsDiv" >
								<div class="option" >
									<input class="brandClass" type="checkbox" value="Samsung" checklist-model="ParamList.brand.choice" ng-click="Search()" />Samsung
									<input class="brandClass" type="checkbox" value="Apple" checklist-model="ParamList.brand.choice" ng-click="Search()" />Apple
									<input class="brandClass" type="checkbox" value="htc" checklist-model="ParamList.brand.choice" ng-click="Search()" />HTC
									<input class="brandClass" type="checkbox" value="huawei" checklist-model="ParamList.brand.choice" ng-click="Search()" />Huawei
								</div>

							</div>
						</div>
					</div>

					<div class="categoryDiv" >
						<div class="innerCategoryDiv" >
							<input class="searchButton" type="button" value="Enter" ng-click="Search()" />
						</div>
					</div>
				</div>

				<div id="anim_div" >
					<input type="button" value="animate me" id="animButton" ></span>
				</div>

			</div>
			<!-- DISPLAYS SEARCH RESULT FROM THE SERVER -->
			<div id= "right_body"  >
				<div class="edge_middle_section"   >
				<ul>
					<li>
						<div class="edge_writeup" >
							<div class="searchResultDiv" >
								<ul >
									<li ng-repeat="result in queryResult" >
										<div class="searchedImage" >
											<img src="images/ensearch_title.png"/>
										</div>
										<div class="searchedName" >
											<span>Name : {{result['name']}}</span>
										</div>
										<div class="searchedCamera" >
											<span>Camera : {{result['f_camera']}}</span>px
										</div>
										<div class="searchedPrice" >
											<span>Price : <span>£</span>{{result['price']}}</span>
										</div>
										<div class="searchedPrice" >
											<span>Year : {{result['year']}}</span>
										</div>
									</li>
								</ul>
							</div>
							
						</div>

					</li>

				</ul>
				</div>
			</div>
			<!-- ACTS AS DEBUG WINDOW, TOGGLED BY CLICKING NAVBAR BUTTON -->
			<div id= "debug_body" ng-show="navChecked"  >
				<div class="edge_middle_section"   >
					<p>{{ newName }}</p>
				</div>
			</div>

		</div>
	</body>
	<!-- FOOTER -->
	<footer>
		<script src="js/index.js" ></script>
		<script src="js/animate_view.js" ></script>
	</footer>
</html>