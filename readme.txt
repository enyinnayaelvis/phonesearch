This is a gadget search engine
Specifically phones at the moment

30/01/15
Going to put in more checking criteria's
Trying to make it dynamic such that it is easy to add other choices as well
Focusing on current criteria's

Price should include
	a number range,
Camera should include
	number of pixels, optical image stabilization, hdr support, raw imaging, etc.
Brand
	could include normal name of brand and also joint branding
Release date
	Year, last month, last three months, etc. It could be last number of __ months
Speed
	CPU SPEED, Geekbench speed, touch response speed, customers review speed


Processor Type
	Brand, type, intricate details of the processor
Display
	Size 	could use a range system for number of inches
	Resolution	Different resolutions with a range system
	Main Display Technology Type, eg super AMOLED, 	retina display, OLED, etc
Memory
	RAM number plus or minus
	Expandable Storage (+or-) with maximum number range
	ROM SIZE - main storage size
Network
	Intricate details of the network
Connectivity
 	(+or-) NFC BLUETOOTH, etc
Operating System
	Type of Operating system, version it comes bundled with
Sensors
	Types of sensors, comes with pro features on
Physical specification
	Dimension and Weight
Battery
	Number for how big, intricate details in pro mode
Formats accepted
	both for audio and video
Others

Build Material
	Material type of body, percentage of material on body


Selections should also be able to tell the user, when whatever they tick does not
affect the selection result.
